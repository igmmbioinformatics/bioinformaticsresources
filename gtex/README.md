# GTEx 


[GTEx portal home](https://gtexportal.org/home/)

[How to access protected and raw data](https://gtexportal.org/home/protectedDataAccess)


## GTEx Analysis V8 (dbGaP Accession phs000424.v8.p2)

The GTEx Analysis V8 release is the most current release of the GTEx Portal. A local mirror of the publicly available data can be rebuilt using the included script `gtex_analysis_v8_build.sh`, with the exception of datasets *sQTL_all_associations* and *eQTL_all_associations* which were provided by Xinyi Jiang and Veronique Vitart:

To download the files from google cloud, setting up a paid account for google cloud is needed. As a new customer of google cloud, $300 free credits are provided and can be used to download these datasets.

The commands used were: 

```
gsutil -u phdproject-308007 -m cp -r "gs://gtex-resources/GTEx_Analysis_v8_QTLs/GTEx_Analysis_v8_EUR_sQTL_all_associations/" .
gsutil -u phdproject-308007 -m cp -r "gs://gtex-resources/GTEx_Analysis_v8_QTLs/GTEx_Analysis_v8_EUR_eQTL_all_associations/" .
gsutil -u phdproject-308007 -m cp -r "gs://gtex-resources/GTEx_Analysis_v8_QTLs/GTEx_Analysis_v8_sQTL_all_associations/"  .
gsutil -u phdproject-308007 -m cp -r "gs://gtex-resources/GTEx_Analysis_v8_QTLs/GTEx_Analysis_v8_eQTL_all_associations/" . 
```
Gsutil is a tool provided by google for accessing the google cloud platform, and the phdproject-308007 is the billing project Xinyi created. 
A login is required to use gsutil for downloading GTEx data. 
Please see this page for details: <https://cloud.google.com/storage/docs/gsutil_install>
