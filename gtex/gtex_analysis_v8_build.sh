#gtex_build.sh
#download latest version of GTEx V8 (dbGaP Accession phs000424.v8.p2)
#root dir /exports/igmm/eddie/BioinformaticsResources/

mkdir gtex; mkdir gtex/gtex_analysis_v8; cd gtex/gtex_analysis_v8

#Annotations
mkdir annotations; cd annotations

wget https://storage.googleapis.com/gtex_analysis_v8/annotations/GTEx_Analysis_v8_Annotations_SampleAttributesDD.xlsx
wget https://storage.googleapis.com/gtex_analysis_v8/annotations/GTEx_Analysis_v8_Annotations_SubjectPhenotypesDD.xlsx
wget https://storage.googleapis.com/gtex_analysis_v8/annotations/GTEx_Analysis_v8_Annotations_SampleAttributesDS.txt
wget https://storage.googleapis.com/gtex_analysis_v8/annotations/GTEx_Analysis_v8_Annotations_SubjectPhenotypesDS.txt

#RNA-Seq Data
cd ..; mkdir rna_seq_data; cd rna_seq_data

wget https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_reads.gct.gz
wget https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_tpm.gct.gz
wget https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz
wget https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_STARv2.5.3a_junctions.gct.gz
wget https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RSEMv1.3.0_transcript_expected_count.gct.gz
wget https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RSEMv1.3.0_transcript_tpm.gct.gz
wget https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_exon_reads.parquet

gunzip *.gz

#Single-Tissue cis-QTL Data
cd ..; mkdir single_tissue_qtl_data; cd single_tissue_qtl_data

wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/README_eQTL_v8.txt
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_eQTL.tar
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_eQTL_expression_matrices.tar
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_eQTL_covariates.tar.gz
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_sQTL.tar
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_sQTL_phenotype_matrices.tar
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_sQTL_groups.tar.gz
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_sQTL_covariates.tar.gz
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_v8_pb_eQTLs.fdr_0.25.high_confidence_set.xlsx
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_eQTL_EUR.tar
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_sQTL_EUR.tar
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_eQTL_independent.tar
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_sQTL_independent.tar

#eQTL Tissue-Specific All SNP Gene Associations
#see README.md

#sQTL Tissue-Specific All SNP Gene Associations
#see README.md

#Single-Tissue trans-QTL Data
#cd single_tissue_qtl_data

wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_v8_finemapping_CAVIAR.tar
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_v8_finemapping_CaVEMaN.tar
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_v8_finemapping_DAPG.tar



#Single-Tissue trans-QTL Data
#cd single_tissue_qtl_data

wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_trans_eGenes_fdr05.txt
wget https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_trans_sGenes_fdr05.txt

for file in *.tar; do tar -xf "$file"; done
for file in *.tar.gz; do tar -zxf "$file"; done

rm *.tar; rm *.tar.gz

gunzip */*gz
rm */*tbi

cd GTEx_v8_finemapping_CAVIAR; tar xf CAVIAR_Results_v8_GTEx_LD_ALL.tar;
rm CAVIAR_Results_v8_GTEx_LD_ALL.tar
cd ..

#Cell Type Interaction QTL Data
cd ..; mkdir interaction_qtl_data; cd interaction_qtl_data

wget https://storage.googleapis.com/gtex_analysis_v8/interaction_qtl_data/GTEx_Analysis_v8_ieQTL.tar
wget https://storage.googleapis.com/gtex_analysis_v8/interaction_qtl_data/GTEx_Analysis_v8_isQTL.tar
wget https://storage.googleapis.com/gtex_analysis_v8/interaction_qtl_data/GTEx_Analysis_v8_xCell_scores_7_celltypes.txt.gz

for file in *.tar; do tar -xf "$file"; done
rm *.tar
gunzip *.gz
gunzip */*gz

#Sex-Biased Genes and sb-eQTL Data
cd ..; mkdir sex_biased_genes_and_sbeqtl_data; cd sex_biased_genes_and_sbeqtl_data

wget https://storage.googleapis.com/gtex_analysis_v8/sex_biased_genes_and_sbeqtl_data/GTEx_Analysis_v8_sbgenes.tar.gz
wget https://storage.googleapis.com/gtex_analysis_v8/sex_biased_genes_and_sbeqtl_data/GTEx_Analysis_v8_sbeQTLs.tar.gz

for file in *.tar.gz; do tar -zxf "$file"; done
rm *.tar.gz

#Multi-Tissue QTL Data
cd ..; mkdir multi_tissue_qtl_data; cd multi_tissue_qtl_data

wget https://storage.googleapis.com/gtex_analysis_v8/multi_tissue_qtl_data/GTEx_Analysis_v8.metasoft.txt.gz
wget https://storage.googleapis.com/gtex_analysis_v8/multi_tissue_qtl_data/gtex_v8_cis_qtls_mashr.tar.gz

for file in *.tar.gz; do tar -zxf "$file"; done
rm *.tar.gz
gunzip *.gz

gunzip */*gz

#Haplotype Expression Matrices
cd ..; mkdir haplotype_expression; cd haplotype_expression

wget https://storage.googleapis.com/gtex_analysis_v8/haplotype_expression/phASER_GTEx_v8_matrix.txt.gz
wget https://storage.googleapis.com/gtex_analysis_v8/haplotype_expression/phASER_GTEx_v8_matrix.gw_phased.txt.gz
wget https://storage.googleapis.com/gtex_analysis_v8/haplotype_expression/phASER_WASP_GTEx_v8_matrix.txt.gz
wget https://storage.googleapis.com/gtex_analysis_v8/haplotype_expression/phASER_WASP_GTEx_v8_matrix.gw_phased.txt.gz
wget https://storage.googleapis.com/gtex_analysis_v8/haplotype_expression/phASER.README.txt

gunzip *.gz

#Outlier Calls
cd ..; mkdir outlier_calls; cd outlier_calls

wget https://storage.googleapis.com/gtex_analysis_v8/outlier_calls/GTEx_v8_outlier_calls.zip

unzip *zip; gunzip *.gz
rm *.zip

#Reference
cd ..; mkdir reference; cd reference

wget https://storage.googleapis.com/gtex_analysis_v8/reference/GTEx_Analysis_2017-06-05_v8_WholeGenomeSeq_838Indiv_Analysis_Freeze.lookup_table.txt.gz
wget https://storage.googleapis.com/gtex_analysis_v8/reference/gencode.v26.GRCh38.genes.gtf
wget https://storage.googleapis.com/gtex_analysis_v8/reference/WGS_Feature_overlap_collapsed_VEP_short_4torus.MAF01.txt.gz

gunzip *.gz

#EOF