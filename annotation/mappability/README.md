# Mappability tracks

## Homo sapiens

### hg38

From: https://genome.ucsc.edu/cgi-bin/hgTrackUi?g=mappability

These tracks indicate regions with uniquely mappable reads of particular lengths before and after bisulfite conversion. Both Umap and Bismap tracks contain single-read mappability and multi-read mappability tracks for four different read lengths: 24 bp, 36 bp, 50 bp, and 100 bp.

You can use these tracks for many purposes, including filtering unreliable signal from sequencing assays. The Bismap track can help filter unreliable signal from sequencing assays involving bisulfite conversion, such as whole-genome bisulfite sequencing or reduced representation bisulfite sequencing.

The files for Homo sapiens build 38 have been downloaded directly from UCSC and are available at `/exports/igmm/eddie/BioinformaticsResources/mappability/homo_sapiens/hg38`.

### hg19

From: https://genome.ucsc.edu/cgi-bin/hgFileUi?db=hg19&g=wgEncodeMapability

These tracks display the level of sequence uniqueness of the reference GRCh37/hg19 genome assembly. They were generated using different window sizes, and high signal will be found in areas where the sequence is unique.

The files for Homo sapiens build 38 have been downloaded directly from UCSC and are available at `/exports/igmm/eddie/BioinformaticsResources/mappability/homo_sapiens/hg19`.

## Other species

If mappability tracks are needed for other species, please contact the BAC. These will be added to `/exports/igmm/eddie/BioinformaticsResources/mappability/genus_species/reference_genome_version`.