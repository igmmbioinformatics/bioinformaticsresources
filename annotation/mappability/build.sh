#!/usr/bin/bash

# hg38
mkdir -p /exports/igmm/eddie/BioinformaticsResources/annotation/mappability/hg38
cd /exports/igmm/eddie/BioinformaticsResources/annotation/mappability/hg38

wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k24.Bismap.MultiTrackMappability.bw
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k24.C2T-Converted.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k24.G2A-Converted.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k24.Umap.MultiTrackMappability.bw
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k24.Unique.Mappability.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k36.Bismap.MultiTrackMappability.bw
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k36.C2T-Converted.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k36.G2A-Converted.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k36.Umap.MultiTrackMappability.bw
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k36.Unique.Mappability.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k50.Bismap.MultiTrackMappability.bw
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k50.C2T-Converted.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k50.G2A-Converted.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k50.Umap.MultiTrackMappability.bw
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k50.Unique.Mappability.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k100.Bismap.MultiTrackMappability.bw
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k100.C2T-Converted.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k100.G2A-Converted.bb
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k100.Umap.MultiTrackMappability.bw
wget http://hgdownload.soe.ucsc.edu/gbdb/hg38/hoffmanMappability/k100.Unique.Mappability.bb

# hg19
mkdir -p /exports/igmm/eddie/BioinformaticsResources/annotation/mappability/hg19
cd /exports/igmm/eddie/BioinformaticsResources/annotation/mappability/hg19

wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeCrgMapabilityAlign24mer.bigWig
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeCrgMapabilityAlign36mer.bigWig
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeCrgMapabilityAlign40mer.bigWig
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeCrgMapabilityAlign50mer.bigWig
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeCrgMapabilityAlign75mer.bigWig
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeCrgMapabilityAlign100mer.bigWig
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeDacMapabilityConsensusExcludable.bed.gz
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeDukeMapabilityRegionsExcludable.bed.gz
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeDukeMapabilityUniqueness20bp.bigWig
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeDukeMapabilityUniqueness35bp.bigWig

gunzip *.gz
