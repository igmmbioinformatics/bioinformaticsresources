# LiftOver chain files

https://genome.ucsc.edu/goldenPath/help/hgTracksHelp.html#Liftover

Chain files mapping coordinates from one genome reference to another are used to 'lift' coordinates of genomic data between references. Primarily this will be between reference assemblies for the same species, but there are between-species chains available as well.

Chain files for going between the most recent 2 human reference assemblies (hg19/GRCh37 and hg38) and the most recent 3 mouse reference assemblies (mm9/GRCm37, mm10/GRCm38, mm39) are available at `/exports/igmm/eddie/BioinformaticsResources/annotation/liftover`.
