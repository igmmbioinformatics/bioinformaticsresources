#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe sharedmem 1
#$ -l h_vmem=4g
#$ -l h_rt=12:00:00

cd /exports/igmm/eddie/BioinformaticsResources/annotation/CADD/CADD_v1.6_GRCh38_SNP
wget -nvc --no-check-certificate https://kircherlab.bihealth.org/download/CADD/v1.6/GRCh38/whole_genome_SNVs.tsv.gz
wget -nvc --no-check-certificate https://kircherlab.bihealth.org/download/CADD/v1.6/GRCh38/whole_genome_SNVs.tsv.gz.tbi
wget -nvc --no-check-certificate https://kircherlab.bihealth.org/download/CADD/v1.6/GRCh38/MD5SUMs

chmod 444 whole_genome_SNVs.tsv.gz
chmod 444 whole_genome_SNVs.tsv.gz.tbi
chmod 444 MD5SUMs

# md5 check of the file(s)
md5sum --check MD5SUMs >> md5_check.txt

# upon completion
#    manually check the md5_check.txt file - all lines must end with OK
#    delete the log files
