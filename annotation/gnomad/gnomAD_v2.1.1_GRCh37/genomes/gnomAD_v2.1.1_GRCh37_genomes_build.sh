#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe sharedmem 1
#$ -l h_vmem=4g
#$ -l h_rt=06:00:00

# before running this script
#    manually create a md5sums.txt file in the format: md5sum (from the gnomAD website) <tab> file_name

cd /exports/igmm/eddie/BioinformaticsResources/annotation/gnomad/gnomAD_v2.1.1_GRCh37/genomes
wget -nv https://storage.googleapis.com/gcp-public-data--gnomad/release/2.1.1/vcf/genomes/gnomad.genomes.r2.1.1.sites.vcf.bgz
wget -nv https://storage.googleapis.com/gcp-public-data--gnomad/release/2.1.1/vcf/genomes/gnomad.genomes.r2.1.1.sites.vcf.bgz.tbi

chmod 444 gnomad.genomes.r2.1.1.sites.vcf.bgz
chmod 444 gnomad.genomes.r2.1.1.sites.vcf.bgz.tbi

# md5 check of the bgz file(s)
md5sum --check md5sums.txt >> md5_check.txt

# upon completion
#    manually check the md5_check.txt file - all lines must end with OK
#    delete the log files

wget https://storage.googleapis.com/gcp-public-data--gnomad/release/2.1.1/vcf/genomes/gnomad.genomes.r2.1.1.exome_calling_intervals.sites.vcf.bgz
wget https://storage.googleapis.com/gcp-public-data--gnomad/release/2.1.1/vcf/genomes/gnomad.genomes.r2.1.1.exome_calling_intervals.sites.vcf.bgz.tbi

