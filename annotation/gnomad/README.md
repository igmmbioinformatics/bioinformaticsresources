# gnomAD data description

The source of gnomAD datasets is https://gnomad.broadinstitute.org

The Genome Aggregation Database (gnomAD), is a coalition of investigators seeking to aggregate and harmonize exome and genome sequencing data from a variety of large-scale sequencing projects, and to make summary data available for the wider scientific community. The project is overseen by co-directors Heidi Rehm and Mark Daly, and steering committee members Daniel MacArthur, Benjamin Neale, Michael Talkowski, Anne O'Donnell-Luria, Konrad Karczewski, Grace Tiao, Matthew Solomonson, and Samantha Baxter.

The data released by gnomAD are available free of restrictions under the Creative Commons Zero Public Domain Dedication. This means that you can use it for any purpose without legally having to give attribution. However, we request that you actively acknowledge and give attribution to the gnomAD project, and link back to the relevant page, wherever possible.


# gnomAD datasets

* The gnomAD v2.1.1 data set contains data from 125,748 exomes and 15,708 whole genomes, all mapped to the GRCh37/hg19 reference sequence.
We refer to this dataset as **gnomAD_v2.1.1_GRCh37** and the data are stored in /exports/igmm/eddie/BioinformaticsResources/annotation/gnomad/gnomAD_v2.1.1_GRCh37/[exomes|genomes]

* The gnomAD v2.1.1 liftover data set contains data from 125,748 exomes and 15,708 whole genomes, lifted over from the GRCh37 to the GRCh38 reference sequence. 
We refer to this dataset as **gnomAD_v2.1.1_GRCh38** and the data are stored in /exports/igmm/eddie/BioinformaticsResources/annotation/gnomad/gnomAD_v2.1.1_GRCh38/[exomes|genomes]

* The gnomAD v3.1.1 data set contains 76,156 whole genomes (and no exomes), all mapped to the GRCh38 reference sequence. 
We refer to this dataset as **gnomAD_v3.1.1_GRCh38** and the data are /exports/igmm/eddie/BioinformaticsResources/annotation/gnomad/gnomAD_v3.1.1_GRCh38/genomes

* The gnomAD v4.1.0 data set contains data from 730,947 exomes and 76,215 whole genomes, all mapped to the GRCh38 reference sequence. We refer to this dataset as **gnomAD_v4.1.0_GRCh38** and the data are /exports/igmm/eddie/BioinformaticsResources/annotation/gnomad/gnomAD_v4.1.0_GRCh38/genomes
