The gnomAD v3.1.1 data set contains 76,156 whole genomes (and no exomes), all mapped to the GRCh38 reference sequence.

* Note to maintainers: gnomAD website does not provide md5sum files at the moment (04/05/2021), but rather lists the md5sum values for *.bgz files. Before running the build.sh file, need to manually create an md5sums.txt file (see the build.sh).