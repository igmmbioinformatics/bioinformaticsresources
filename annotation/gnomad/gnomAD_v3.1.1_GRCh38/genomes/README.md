Data downloaded from https://gnomad.broadinstitute.org/downloads on 04/05/2021.
The variant dataset files below contain all subsets (non-cancer, non-neuro, non-v2, non-TOPMed, controls/biobanks, 1KG, and HGDP).

For this dataset, the data is split per chromosome.

Additionally:
- variants per chromosome failing gnomAD filtering are extracted with extract_FAIL_per_chr.sh into gnomad.genomes.v3.1.1.FAIL.sites.${CHR}.txt
- coverage information is downloaded with gnomAD_v3.1.1_GRCh38_genomes_download_coverage.sh in a file called gnomad.genomes.r3.0.1.coverage.summary.tsv.bgz 