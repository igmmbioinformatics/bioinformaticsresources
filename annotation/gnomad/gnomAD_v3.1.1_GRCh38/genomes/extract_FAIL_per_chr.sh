#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe sharedmem 1
#$ -l h_vmem=4g
#$ -l h_rt=12:00:00
#

# Configure modules
. /etc/profile.d/modules.sh
MODULEPATH=$MODULEPATH:/exports/igmm/software/etc/el7/modules
#module load igmm/apps/tabix/0.2.6


PARAMS=$1
CHR=$(cat $PARAMS | head -n $SGE_TASK_ID | tail -n 1 | awk '{ print $1 }')


cd /exports/igmm/eddie/BioinformaticsResources/annotation/gnomad/gnomAD_v3.1.1_GRCh38/genomes


#################################################
### extract variants failing gnomAD filtering ###
#################################################

time zgrep -v '^#' gnomad.genomes.v3.1.1.sites.${CHR}.vcf.bgz | awk -v OFS='\t' '$7 != "PASS" \
{print $1, $2, $4, $5, $7}' > gnomad.genomes.v3.1.1.FAIL.sites.${CHR}.txt

chmod 444 gnomad.genomes.v3.1.1.FAIL.sites.${CHR}.txt

echo "${CHR}: Variants failing gnomAD filtering are extracted"
