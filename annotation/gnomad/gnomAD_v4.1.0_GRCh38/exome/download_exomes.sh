#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=4GB
#SBATCH --time=168:00:00
#SBATCH --job-name=down_gnomADe4.1.0
#SBATCH --output=down_gnomADe4.1.0.out
#SBATCH --error=down_gnomADe4.1.0.err


#       Author: MH
#       last modified: JUNE 05, 2024

#       see https://gnomad.broadinstitute.org/downloads#v4-core-dataset
#       The gnomAD v4.1.0 data set contains data from 730,947 exomes and 76,215 whole genomes, all mapped to the GRCh38 reference sequence.

# download files
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr1.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr2.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr3.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr4.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr5.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr6.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr7.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr8.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr9.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr10.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr11.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr12.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr13.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr14.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr15.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr16.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr17.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr18.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr19.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr20.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr21.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr22.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chrX.vcf.bgz
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chrY.vcf.bgz


# download indexes
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr1.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr2.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr3.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr4.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr5.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr6.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr7.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr8.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr9.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr10.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr11.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr12.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr13.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr14.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr15.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr16.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr17.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr18.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr19.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr20.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr21.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chr22.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chrX.vcf.bgz.tbi
time wget https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/gnomad.exomes.v4.1.sites.chrY.vcf.bgz.tbi
