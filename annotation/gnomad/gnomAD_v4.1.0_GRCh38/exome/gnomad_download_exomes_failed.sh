#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=4GB
#SBATCH --time=24:00:00
#SBATCH --job-name=retry_exome_downloads
#SBATCH --output=retry_exome_downloads.out
#SBATCH --error=retry_exome_downloads.err

# Author: SFox
# Script to re-download failed gnomAD exome files and verify their MD5 checksums
# Files were failing md5 checksum regularly, so added Retry mechanism

# Directory where the files will be downloaded
DOWNLOAD_DIR="/exports/igmm/eddie/BioinformaticsResources/annotation/gnomad/gnomAD_v4.1.0_GRCh38/exome"

# URL base for downloading the files
BASE_URL="https://datasetgnomad.blob.core.windows.net/dataset/release/4.1/vcf/exomes/"

# Define files and their expected MD5 checksums
declare -A files_to_download=(
    ["gnomad.exomes.v4.1.sites.chr3.vcf.bgz"]="a35b949b32453b4b5abd7b1de42e298a"
)

# Function to download file and check MD5
download_and_verify() {
    local file=$1
    local expected_md5=$2
    local url="${BASE_URL}${file}"

    echo "Downloading $file"
    wget -O "${DOWNLOAD_DIR}/${file}" "$url"

    # Verify MD5 checksum
    echo "Verifying MD5 checksum for $file"
    local computed_md2=$(md5sum ${DOWNLOAD_DIR}/${file} | awk '{print $1}')

    if [[ "$computed_md2" == "$expected_md5" ]]; then
        echo "$file MD5 checksum verification PASSED."
    else
        echo "$file MD5 checksum verification FAILED."
        echo "Expected: $expected_md5"
        echo "Computed: $computed_md2"
        return 1 # Return a failure status
    fi
}

# Retry mechanism
for file in "${!files_to_download[@]}"; do
    attempts=0
    max_attempts=3
    until (( attempts >= max_attempts ))
    do
        download_and_verify "$file" "${files_to_download[$file]}"
        [[ $? == 0 ]] && break # Stop if successful
        ((attempts++))
        echo "Retrying $file ($((attempts+1)) attempt)..."
        sleep 5 # Wait 5 seconds before retrying
    done

    if (( attempts == max_attempts )); then
        echo "Failed to download and verify $file after $max_attempts attempts."
    fi
done

echo "Exome download and verification tasks completed."