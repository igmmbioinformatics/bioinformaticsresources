# Ensembl VEP

https://www.ensembl.org/info/docs/tools/vep/index.html

This annotation tool is in the module system on eddie. Use the command 

```
module avail igmm/apps/vep
```

to find which versions are installed. The versions correspond to the Ensembl genome build versions. If you need a more recent version, please open an [IS helpline ticket](https://ed.unidesk.ac.uk/tas/public/ssp/) and put 'IGC - VEP update' in the subject.

Research computing installs the VEP caches for:

* homo_sapiens: GRCh37, GRCh38 (build based on only the Ensembl transcripts)
* homo_sapiens_refseq: GRCh37, GRCh38 (build based on only RefSeq transcripts)
* homo_sapiens_merged: GRCh37, GRCh38 (build based on a merger of Ensembl and RefSeq transcripts)

To use these caches, set the `version` environment variable to the version you want to use, as in the example below, e.g. 102.

```
version=102
module load igmm/apps/vep/$version
vep --dir_cache /exports/igmm/software/pkg/el7/apps/vep/$version/cache --offline <remaining_parameters>
```

If you want to use VEP for a non-human species, please contact the BAC who will download this for you, then run VEP pointing to the cache directory on `BioinformaticsResources`.

```
version=102
module load igmm/apps/vep/$version
vep --dir_cache /exports/igmm/eddie/BioinformaticsResources/annotation/vep --offline <remaining_parameters>
```

Please note that this cache may contain caches for multiple versions of VEP. Ensure that the version of VEP you are running matches the cache version for your species.

```
ls /exports/igmm/eddie/BioinformaticsResources/annotation/vep/genus_species
```
