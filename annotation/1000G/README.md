# 1000 Genomes Project 

The most contemporary release of 1000G variant callset is provided by NYGC https://www.ebi.ac.uk/ena/browser/view/PRJEB31736.

It offers several advantages over the original 1000G phase 3 release, including: all samples sequenced to a minimum of 30x mean genome coverage (deeper coverage), using PCR-free sequencing libraries (avoiding PCR artefacts), sequenced on the Illumina NovaSeq 6000 sequencing instrument with 2x150bp reads (longer read length compared to the original) and aligned directly to the hg38 reference (therefore avoiding the known liftover issues).

* Downloaded from https://www.internationalgenome.org/data-portal/data-collection/30x-grch38

* This variant set contains 2504 unrelated individuals from 26 populations. 
The list of all the samples in the data set and their population, super population and gender were obtained from ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ and can be found in the file `integrated_call_samples_v3.20130502.ALL.panel`.
Individual counts by super-pop: AFR = 661, AMR = 347, EAS = 504, EUR = 503 and SAS = 489.

* Details of the analyses and the pipeline can be found at https://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000G_2504_high_coverage/20190405_NYGC_b38_pipeline_description.pdf

* For this dataset, the data is split per chromosome.

* Note to maintainers: the download speed fluctuates widely.