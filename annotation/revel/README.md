REVEL is an ensemble method for predicting the pathogenicity of missense variants based on a combination of scores from 13 individual tools:

1.  MutPred, 
1.  FATHMM v2.3, 
1.  VEST 3.0, 
1.  PolyPhen-2, 
1.  SIFT, 
1.  PROVEAN, 
1.  MutationAssessor, 
1.  MutationTaster, 
1.  LRT,
1.  GERP++, 
1.  SiPhy, 
1.  phyloP, and 
1.  phastCons. 


## Revel files

The file `revel_with_transcript_ids`  is in comma-separated value (CSV) format and contain 7 fields:

1. chr: Chromosome
1. hg19_pos: Sequence position on the hg19 (GRCh37) human genome build (1-based coordinates)
1. grch38_pos: Sequence position on the GRCh38 human genome build (1-based coordinates).  Value is "." when there is no corresponding GRCh38 position for the variant.
1. ref: Reference nucleotide
1. alt: Alternate nucleotide
1. aaref: Reference amino acid
1. aaalt: Alternate amino acid
1. REVEL: REVEL score
1. Ensembl_transcriptid: Ensemble transcript ID(s) corresponding to the given amino acid substitution (multiple transcript IDs are delimited by ";")

Example below

```bash
chr,hg19_pos,grch38_pos,ref,alt,aaref,aaalt,REVEL,Ensembl_transcriptid
1,35142,35142,G,A,T,M,0.027,ENST00000417324
1,35142,35142,G,C,T,R,0.035,ENST00000417324
1,35142,35142,G,T,T,K,0.043,ENST00000417324
1,35143,35143,T,A,T,S,0.018,ENST00000417324
1,35143,35143,T,C,T,A,0.034,ENST00000417324
1,35143,35143,T,G,T,P,0.039,ENST00000417324
1,35144,35144,A,C,C,W,0.012,ENST00000417324
1,35145,35145,C,A,C,F,0.023,ENST00000417324

```

