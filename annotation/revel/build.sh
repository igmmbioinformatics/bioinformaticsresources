#!/usr/bin/bash

# make directory structure
mkdir -p /exports/igmm/eddie/BioinformaticsResources/annotation/revel/v1.3
cd /exports/igmm/eddie/BioinformaticsResources/annotation/revel/v1.3

#download data
curl -L -o revel-v1.3_all_chromosomes.zip https://rothsj06.dmz.hpc.mssm.edu/revel-v1.3_all_chromosomes.zip
# unzip
unzip revel-v1.3_all_chromosomes.zip
# remove zip file
rm  revel-v1.3_all_chromosomes.zip

#change permissions
chmod -R g+rx ../../revel/
