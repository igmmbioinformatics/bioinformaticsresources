# Shared Package cache for conda

To  improve the speed at which packages are installed or new environments are created and save disk space 
I have by set up a shared package cache.

https://docs.anaconda.com/anaconda/user-guide/tasks/shared-pkg-cache/

## Adding shared package cache

To add the shared pkg cache run this following conda command

~~~
conda config --add pkgs_dirs /exports/igmm/eddie/BioinformaticsResources/conda/pkgs_dirs
~~~

## Permission

