# load module
module load igmm/apps/refgenie/0.12.0
#initial configuration
#refgenie init -c /exports/igmm/eddie/BioinformaticsResources/refgenie/genome_config.yaml


#modules
module load roslin/samtools/1.10


#Build assets
#https://www.ensembl.org/info/data/ftp/index.html


#Human

#GRCh38_primary
##fasta
wget http://ftp.ensembl.org/pub/release-103/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
refgenie build GRCh38/fasta:primary_assembly --files fasta=Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz -R
rm Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz

## fasta_txome
### 104
wget http://ftp.ensembl.org/pub/release-104/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz
refgenie build GRCh38/fasta_txome:104 --files fasta=Homo_sapiens.GRCh38.cdna.all.fa.gz
rm Homo_sapiens.GRCh38.cdna.all.fa.gz

##ensembl_gtf
wget http://ftp.ensembl.org/pub/release-104/gtf/homo_sapiens/Homo_sapiens.GRCh38.104.gtf.gz
refgenie build GRCh38/ensembl_gtf:104  --files ensembl_gtf=Homo_sapiens.GRCh38.104.gtf.gz
rm Homo_sapiens.GRCh38.104.gtf.gz

#salmon_index
module load igmm/apps/salmon/0.14.1
refgenie build GRCh38/salmon_index:0.14.1 --assets fasta=GRCh38/fasta_txome:104

#Mouse

# GRCm39_primary
## fasta
wget http://ftp.ensembl.org/pub/release-103/fasta/mus_musculus/dna/Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
refgenie build GRCm39_primary/fasta:103 --files fasta=Mus_musculus.GRCm39.dna.primary_assembly.fa.gz -R
rm Mus_musculus.GRCm39.dna.primary_assembly.fa.gz

##ensembl_gtf
wget http://ftp.ensembl.org/pub/release-103/gtf/mus_musculus/Mus_musculus.GRCm39.103.gtf.gz
refgenie build  GRCm39_primary/ensembl_gtf:103 --files ensembl_gtf=Mus_musculus.GRCm39.103.gtf.gz
rm Mus_musculus.GRCm39.103.gtf.gz


##fasta_txome
wget http://ftp.ensembl.org/pub/release-103/fasta/mus_musculus/cdna/Mus_musculus.GRCm39.cdna.all.fa.gz
refgenie build GRCm39_primary/fasta_txome:103 --files fasta=Mus_musculus.GRCm39.cdna.all.fa.gz
rm Mus_musculus.GRCm39.cdna.all.fa.gz


# GRCm38
## FASTA
wget ftp://ftp.ensembl.org/pub/release-75/fasta/mus_musculus/dna/Mus_musculus.GRCm38.75.dna.primary_assembly.fa.gz
refgenie build GRCm38_primary/fasta:primary_assembly --files fasta=Mus_musculus.GRCm38.75.dna.primary_assembly.fa.gz -R
rm Mus_musculus.GRCm38.75.dna.primary_assembly.fa.gz



##fasta_txome
wget ftp://ftp.ensembl.org/pub/release-75/fasta/mus_musculus/cdna/Mus_musculus.GRCm38.75.cdna.all.fa.gz
refgenie build GRCm38_primary/fasta_txome:75 --files fasta=Mus_musculus.GRCm38.75.cdna.all.fa.gz
rm Mus_musculus.GRCm38.75.cdna.all.fa.gz

##ensembl_gtf
wget ftp://ftp.ensembl.org/pub/release-75/gtf/mus_musculus/Mus_musculus.GRCm38.75.gtf.gz
refgenie build GRCm38_primary/ensembl_gtf:75 --files ensembl_gtf=Mus_musculus.GRCm38.75.gtf.gz
rm Mus_musculus.GRCm38.75.gtf.gz


# Dog

# CanFam3 toplevel sm
## fasta
wget http://ftp.ensembl.org/pub/release-103/fasta/canis_lupus_familiaris/dna/Canis_lupus_familiaris.CanFam3.1.dna_sm.toplevel.fa.gz
refgenie build CanFam3_toplevel/fasta:103 --files fasta=Canis_lupus_familiaris.CanFam3.1.dna_sm.toplevel.fa.gz -R
rm Canis_lupus_familiaris.CanFam3.1.dna_sm.toplevel.fa.gz

##ensembl_gtf
wget http://ftp.ensembl.org/pub/release-103/gtf/canis_lupus_familiaris/Canis_lupus_familiaris.CanFam3.1.103.gtf.gz
refgenie build  CanFam3_toplevel/ensembl_gtf:103 --files ensembl_gtf=Canis_lupus_familiaris.CanFam3.1.103.gtf.gz
rm Canis_lupus_familiaris.CanFam3.1.103.gtf.gz


##fasta_txome
wget http://ftp.ensembl.org/pub/release-103/fasta/canis_lupus_familiaris/cdna/Canis_lupus_familiaris.CanFam3.1.cdna.all.fa.gz
refgenie build CanFam3_toplevel/fasta_txome:103 --files fasta=Canis_lupus_familiaris.CanFam3.1.cdna.all.fa.gz
rm Canis_lupus_familiaris.CanFam3.1.cdna.all.fa.gz




#104

wget http://ftp.ensembl.org/pub/release-104/fasta/canis_lupus_familiaris/dna/Canis_lupus_familiaris.CanFam3.1.dna_sm.toplevel.fa.gz
refgenie build CanFam3/fasta:toplevel --files fasta=Canis_lupus_familiaris.CanFam3.1.dna_sm.toplevel.fa.gz -R
rm Canis_lupus_familiaris.CanFam3.1.dna_sm.toplevel.fa.gz

##ensembl_gtf
wget http://ftp.ensembl.org/pub/release-104/gtf/canis_lupus_familiaris/Canis_lupus_familiaris.CanFam3.1.104.gtf.gz
refgenie build  CanFam3/ensembl_gtf:104 --files ensembl_gtf=Canis_lupus_familiaris.CanFam3.1.104.gtf.gz
rm Canis_lupus_familiaris.CanFam3.1.104.gtf.gz


##fasta_txome
wget http://ftp.ensembl.org/pub/release-104/fasta/canis_lupus_familiaris/cdna/Canis_lupus_familiaris.CanFam3.1.cdna.all.fa.gz
refgenie build CanFam3/fasta_txome:104 --files fasta=Canis_lupus_familiaris.CanFam3.1.cdna.all.fa.gz
rm Canis_lupus_familiaris.CanFam3.1.cdna.all.fa.gz

