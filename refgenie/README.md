# What is refgenie?

Refgenie manages storage, access, and transfer of reference genome resources. 
It provides command-line and Python interfaces to download pre-built reference genome "assets", like indexes used by bioinformatics tools. 
It can also build assets for custom genome assemblies. 
Refgenie provides programmatic access to a standard genome folder structure, so software can swap from one genome to another.


http://refgenie.databio.org/en/latest/

## Setup

The refgenie commands all require knowing where this genome config file is. You can pass it on the command line all the time (using the -c parameter), 
but this gets old. An alternative is to set up the $REFGENIE environment variable like so:

~~~
export REFGENIE=/exports/igmm/eddie/BioinformaticsResources/refgenie/genome_config.yaml
~~~

## Build script

The `build.sh` bash script is the commands used to download and build genome assets.

## Tutorial

http://refgenie.databio.org/en/latest/tutorial/

##   Local refgenie assets

~~~
                        Local refgenie assets
         Server subscriptions: http://refgenomes.databio.org
┏━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ genome               ┃ assets                                      ┃
┡━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│ GRCm39_primary       │ fasta, ensembl_gtf, fasta_txome             │
│ GRCh38_primary       │ fasta, ensembl_gtf, fasta_txome             │
│ GRCm38_primary       │ fasta, ensembl_gtf, fasta_txome             │
└──────────────────────┴─────────────────────────────────────────────┘
~~~

## Tagging assets

The tag command allows users to tag assets with unique identifiers. 

Tags may also be provided when building or pulling assets to specify a version 

e.g; 

```
$refgenie build hg38/ASSET:TAG. 
```
Once tagged, specific versions of assets can be accessed by tag. 

If no tag is specified, refgenie will use the tag default, which is automatically given to any built or pulled assets that do not specify a tag. 

This makes tags an optional feature of refgenie that are only necessary if a user desires multiple versions of the same asset.




## Commands

~~~
version: 0.11.0 | refgenconf 0.11.0
usage: refgenie [-h] [--version] [--silent] [--verbosity V] [--logdev] {init,list,listr,pull,build,seek,seekr,add,remove,getseq,tag,id,subscribe,unsubscribe,alias,compare,upgrade,populate,populater} ...

refgenie - reference genome asset manager

positional arguments:
  {init,list,listr,pull,build,seek,seekr,add,remove,getseq,tag,id,subscribe,unsubscribe,alias,compare,upgrade,populate,populater}
    init                Initialize a genome configuration.
    list                List available local assets.
    listr               List available remote assets.
    pull                Download assets.
    build               Build genome assets.
    seek                Get the path to a local asset.
    seekr               Get the path to a remote asset.
    add                 Add local asset to the config file.
    remove              Remove a local asset.
    getseq              Get sequences from a genome.
    tag                 Tag an asset.
    id                  Return the asset digest.
    subscribe           Add a refgenieserver URL to the config.
    unsubscribe         Remove a refgenieserver URL from the config.
    alias               Interact with aliases.
    compare             Compare two genomes.
    upgrade             Upgrade config. This will alter the files on disk.
    populate            Populate registry paths with local paths.
    populater           Populate registry paths with remote paths.

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  --silent              Silence logging. Overrides verbosity.
  --verbosity V         Set logging level (1-5 or logging module level name)
  --logdev              Expand content of logging message format.
  ~~~
  
  
  
  ## finding assets
  ~~~
  refgenie list 
  refgenie list -g CanFam3_toplevel 
  refgenie seek -g  CanFam3_toplevel -a fasta_txome.fai:104
  ~~~