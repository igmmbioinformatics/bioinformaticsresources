# iGenomes

# iGenomes (https://support.illumina.com/sequencing/sequencing_software/igenome.html) is Illumina's reference genome
# service. On top of this, SciLifeLabs have a download site and script at https://ewels.github.io/AWS-iGenomes, to sync
# only the bits we want from AWS.

module load igmm/apps/awscli/2.1.6
cd /exports/igmm/eddie/BioinformaticsResources/igenomes
wget https://ewels.github.io/AWS-iGenomes/aws-igenomes.sh

log_file=aws-igenomes.log

# aws-igenomes.sh is hard-coded to download either to `references/$species/$source/$build`, or a specific location,
# i.e. it's not possible to relocate the file structure - so we have to copy some of the script's logic to replicate the
# tree in the right place
type_suffixes=(
    "gtf::Annotation/Genes/"
    "bed12::Annotation/Genes/"
    "bismark::Sequence/BismarkIndex/"
    "bowtie::Sequence/BowtieIndex/"
    "bowtie2::Sequence/Bowtie2Index/"
    "bwa::Sequence/BWAIndex/"
    "star::Sequence/STARIndex/"
    "fasta::Sequence/WholeGenomeFasta/"
    "chromosomes::Sequence/Chromosomes/"
    "abundantseq::Sequence/AbundantSequences/"
    "smrna::Annotation/SmallRNA/"
    "variation::Annotation/Variation/"
    "gatk::"
)

function download_igenome {
    genome=$1
    refsource=$2
    build=$3
    reftype=$4

    for i in "${type_suffixes[@]}"
    do
        k="${i%%::*}"
        v="${i##*::}"
            if [ "$k" == "$reftype" ]
            then
                ref_suffix=$v
            fi
    done
    if [ "$ref_suffix" == 'unset' ]
    then
        echo "Error, could not find reference suffix" >&2
        exit 1
    fi

    output_dir="$genome/$refsource/$build/$ref_suffix"
    sh aws-igenomes.sh -g $species -s $refsource -b $build -t $reftype -o $output_dir
}

# Download the specified datasets for:
# - Human:
#   - UCSC:
#     - hg39
#     - hg19
# - Mouse:
#   - Ensembl:
#     - GRCh37
for reftype in fasta chromosomes bwa star bowtie2 gtf bismark smrna variation
do
    download_igenome Homo_sapiens UCSC hg38 $reftype >> $log_file 2>&1
    download_igenome Homo_sapiens UCSC hg19 $reftype >> $log_file 2>&1
    download_igenome Homo_sapiens Ensembl GRCh37 $reftype >> $log_file 2>&1

for reftype in fasta chromosomes bwa star bowtie2 gtf bismark smrna
do
    download_igenome Mus_musculus Ensembl GRCm38 $reftype >> $log_file 2>&1
done


for reftype in fasta chromosomes bwa star bowtie2 gtf bismark smrna
do
    download_igenome Danio_rerio Ensembl GRCz10 $reftype >> $log_file 2>&1
done

## ecoli

aws s3 --no-sign-request --region eu-west-1 sync s3://ngi-igenomes/igenomes/Escherichia_coli_K_12_MG1655/NCBI/2001-10-15/Sequence/Bowtie2Index/ Escherichia_coli_K_12_MG1655/NCBI/2001-10-15/Sequence/Bowtie2Index/

# Building GRCh38 STAR index files for nf-core RNAseq use

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl
mkdir -p /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/{Annotation,Sequence}
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/WholeGenomeFasta
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Annotation/Genes

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/WholeGenomeFasta
wget http://ftp.ensembl.org/pub/release-103/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
gunzip Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz>genome.fa

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Annotation/Genes
wget http://ftp.ensembl.org/pub/release-103/gtf/homo_sapiens/Homo_sapiens.GRCh38.103.gtf.gz
gunzip Homo_sapiens.GRCh38.103.gtf.gz>genes.gtf

module load anaconda/5.3.1

conda create -n star261d-env star=2.6.1d
conda activate star261d-env

mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/STARIndex
cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/

STAR --runMode genomeGenerate --genomeDir STARIndex --genomeFastaFiles \
WholeGenomeFasta/genome.fa --sjdbGTFfile \
../Annotation/Genes/genes.gtf --sjdbOverhang 100

# Building GRCm39 STAR index files for nf-core RNAseq use

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Mus_musculus/Ensembl
mkdir -p /exports/igmm/eddie/BioinformaticsResources/igenomes/Mus_musculus/Ensembl/GRCm39/{Annotation,Sequence}
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Mus_musculus/Ensembl/GRCm39/Sequence/WholeGenomeFasta
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Mus_musculus/Ensembl/GRCm39/Annotation/Genes

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Mus_musculus/Ensembl/GRCm39/Sequence/WholeGenomeFasta
wget http://ftp.ensembl.org/pub/release-103/fasta/mus_musculus/dna/Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
gunzip Homo_sapiens.GRCm39.dna.primary_assembly.fa.gz>genome.fa

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Mus_musculus/Ensembl/GRCm39/Annotation/Genes
wget http://ftp.ensembl.org/pub/release-103/gtf/mus_musculus/Mus_musculus.GRCm39.103.gtf.gz
gunzip Mus_musculus.GRCm39.103.gtf.gz>genes.gtf

module load anaconda/5.3.1

conda activate star261d-env

mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Mus_musculus/Ensembl/GRCm39/Sequence/STARIndex
cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Mus_musculus/Ensembl/GRCm39/Sequence/

STAR --runMode genomeGenerate --genomeDir STARIndex --genomeFastaFiles \
WholeGenomeFasta/genome.fa --sjdbGTFfile \
../Annotation/Genes/genes.gtf --sjdbOverhang 100


# Downloading GRCz11 for latest Ensembl build (not in iGenomes)
cd /exports/igmm/eddie/BioinformaticsResources/igenomes
mkdir -p Danio_rerio/Ensembl
cd Danio_rerio/Ensembl
mkdir -p GRCz11_110/Sequence/WholeGenomeFasta
cd GRCz11_110/Sequence/WholeGenomeFasta
wget https://ftp.ensembl.org/pub/release-110/fasta/danio_rerio/dna/Danio_rerio.GRCz11.dna.primary_assembly.fa.gz
gzip -d Danio_rerio.GRCz11.dna.primary_assembly.fa.gz
cd ../../Sequence
conda activate /exports/igmm/eddie/BioinformaticsResources/conda/envs/star261d
mkdir STARIndex
STAR --runMode genomeGenerate --genomeDir STARIndex --genomeFastaFiles WholeGenomeFasta/Danio_rerio.GRCz11.dna.primary_assembly.fa --sjdbGTFfile ../Annotation/Genes/Danio_rerio.GRCz11.110.chr.gtf --sjdbOverhang 100
cd /exports/igmm/eddie/BioinformaticsResources/igenomes

# Building Dog CanFam3.1 STAR index files for nf-core RNAseq use

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Canis_lupus_familiaris/Ensembl
mkdir -p /exports/igmm/eddie/BioinformaticsResources/igenomes/Canis_lupus_familiaris/Ensembl/CanFam3.1/{Annotation,Sequence}
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Canis_lupus_familiaris/Ensembl/CanFam3.1/Sequence/WholeGenomeFasta
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Canis_lupus_familiaris/Ensembl/CanFam3.1/Annotation/Genes

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Canis_lupus_familiaris/Ensembl/CanFam3.1/Sequence/WholeGenomeFasta
wget http://ftp.ensembl.org/pub/release-104/fasta/canis_lupus_familiaris/dna/Canis_lupus_familiaris.CanFam3.1.dna.toplevel.fa.gz
gunzip Canis_lupus_familiaris.CanFam3.1.dna.toplevel.fa.gz>genome.fa

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Canis_lupus_familiaris/Ensembl/CanFam3.1/Annotation/Genes
wget http://ftp.ensembl.org/pub/release-104/gtf/canis_lupus_familiaris/Canis_lupus_familiaris.CanFam3.1.104.gtf.gz
gunzip Canis_lupus_familiaris.CanFam3.1.104.gtf.gz>genes.gtf

module load anaconda/5.3.1

conda activate star261d-env

mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Canis_lupus_familiaris/Ensembl/CanFam3.1/Sequence/STARIndex
cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Canis_lupus_familiaris/Ensembl/CanFam3.1/Sequence/

STAR --runMode genomeGenerate --genomeDir STARIndex --genomeFastaFiles \
WholeGenomeFasta/genome.fa --sjdbGTFfile \
../Annotation/Genes/genes.gtf --sjdbOverhang 100



# Building GRCh38 STAR index files for nf-core RNAseq use

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl
mkdir -p /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/{Annotation,Sequence}
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/WholeGenomeFasta
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Annotation/Genes

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/WholeGenomeFasta
wget http://ftp.ensembl.org/pub/release-103/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
gunzip Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz>genome.fa

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Annotation/Genes
wget http://ftp.ensembl.org/pub/release-103/gtf/homo_sapiens/Homo_sapiens.GRCh38.103.gtf.gz
gunzip Homo_sapiens.GRCh38.103.gtf.gz>genes.gtf

module load anaconda/5.3.1

conda create -n star261d-env star=2.6.1d
conda activate star261d-env

mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/STARIndex
cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/

STAR --runMode genomeGenerate --genomeDir STARIndex --genomeFastaFiles \
WholeGenomeFasta/genome.fa --sjdbGTFfile \
../Annotation/Genes/genes.gtf --sjdbOverhang 100

# Building GRCz10 STAR index files for nf-core RNAseq use

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Danio_rerio/Ensembl
mkdir -p /exports/igmm/eddie/BioinformaticsResources/igenomes/Danio_rerio/Ensembl/GRCz10/{Annotation,Sequence}
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Danio_rerio/Ensembl/GRCz10/Sequence/WholeGenomeFasta
mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Danio_rerio/Ensembl/GRCz10/Annotation/Genes

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Danio_rerio/Ensembl/GRCz10/Sequence/WholeGenomeFasta
wget http://ftp.ensembl.org/pub/current_fasta/danio_rerio/dna/Danio_rerio.GRCz11.dna.primary_assembly.fa.gz
gunzip Danio_rerio.GRCz11.dna.primary_assembly.fa.gz > genome.fa

# index and create dictionary
module load roslin/samtools/1.9
samtools faidx genome.fa
samtools dict genome.fa

cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Danio_rerio/Ensembl/GRCz10/Annotation/Genes
wget http://ftp.ensembl.org/pub/release-103/gtf/mus_musculus/Mus_musculus.GRCm39.103.gtf.gz
gunzip Mus_musculus.GRCm39.103.gtf.gz > genes.gtf

module load roslin/star/2.7.10a



mkdir /exports/igmm/eddie/BioinformaticsResources/igenomes/Danio_rerio/Ensembl/GRCz10/Sequence/STARIndex
cd /exports/igmm/eddie/BioinformaticsResources/igenomes/Danio_rerio/Ensembl/GRCz10/Sequence/

STAR --runMode genomeGenerate --genomeDir STARIndex --genomeFastaFiles \
WholeGenomeFasta/genome.fa --sjdbGTFfile \
../Annotation/Genes/genes.gtf --sjdbOverhang 100


