# iGenomes

[iGenomes](https://support.illumina.com/sequencing/sequencing_software/igenome.html) is Illumina's reference genome
service. On top of this, SciLifeLabs have a [download site](https://ewels.github.io/AWS-iGenomes) and script to sync
only the bits we want from AWS.

`build.sh` downloads and runs the `aws-igenomes.sh` download script on some set genomes and indexes.

As Ensembl GRCh38 files and STAR indexes are missing from iGenomes, these are added manually in `build.sh`, mirroring
the existing iGenomes directory structure created by `aws-igenomes.sh`. The star index created for Ensembl GRCh38
(/exports/igmm/eddie/BioinformaticsResources/igenomes/references/Homo_sapiens/Ensembl/GRCh38/Sequence/STARIndex) has
been created specifically to use with Nextflow nf-core and by an older version of star: star 2.6.1d. This star index
will not work in combination with the latest version of star.

A similar process has been applied to download files and build index for Mus musculus GRCm39.

iGenomes GATK files for Human GRCh38 have been added using the aws download script
aws s3 --no-sign-request --region eu-west-1 sync s3://ngi-igenomes/igenomes/Homo_sapiens/GATK/GRCh38/ /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/GATK/GRCh38/
