John Ireland has created a module area for the BAC. Module files go in:

```
/exports/applications/modulefiles/Community/igmm/bac
```

and the applications live in:

```
/exports/igmm/software/pkg/el7/bac
```

There is a copy of `igmm/apps/refgenie` called `igmm/bac/refgenie` that can be used as a template.

For complex modules (which set CFLAGS etc) look at `module show igmm/compilers/gcc`.

Members of igmm_datastore_BioinformaticsResources should be able to edit both module and application areas.

```
$ module show igmm/bac/refgenie
-------------------------------------------------------------------
/exports/applications/modulefiles/Community/igmm/bac/refgenie/0.12.0:

module-whatis      
             Name: refgenie
          Version: 0.12.0
          Summary: Refgenie manages storage, access, and transfer of reference genome resources
              URL: http://refgenie.databio.org/en/latest/
          Builder: Ewan McDowall
       Build date: Thu  2 Sep 15:03:03 BST 2021
 
module         load igmm/apps/python 
module         load igmm/apps/yacman/0.8.2 
setenv         REFGENIEDIR /exports/igmm/software/pkg/el7/bac/refgenie/0.12.0 
setenv         REFGENIEBIN /exports/igmm/software/pkg/el7/bac/refgenie/0.12.0/bin 
setenv         PYTHONPATH /exports/igmm/software/pkg/el7/bac/refgenie/0.12.0/lib/python3.7/site-packages/ 
prepend-path     PATH /exports/igmm/software/pkg/el7/bac/refgenie/0.12.0/bin 
setenv         REFGENIE /exports/igmm/eddie/BioinformaticsResources/refgenie/alias/genome_config.yaml 
-------------------------------------------------------------------
```
