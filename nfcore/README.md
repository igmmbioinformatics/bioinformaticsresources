## nf-core 


### singularity cache

nf-core can use Singularity images for software. 

You can specify a cache directory with the nextflow environment variable `NXF_SINGULARITY_CACHEDIR` 

This should point to the directory with singularity images for the pipeline and version.


#### getting the singularity images

nf-core provides a helper tool nf-core that enables you to download an entire pipeline offline, 

including the singularity images see [here](https://nf-co.re/tools/#downloading-pipelines-for-offline-use)

example
~~~
nf-core -v download nf-core/eager -r 2.3.3 -s 2>&1 > nf_core_log.txt
~~~

## DSL1 pipelines

Some older pipelines had to convert their docker images into singularity images.