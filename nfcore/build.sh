#!/bin/bash
cd /exports/igmm/eddie/BioinformaticsResources/nfcore
#conda create --prefix ./env python=3.7 nf-core nextflow
conda activate ./env
module load roslin/singularity/3.5.3

##############################################################################
#                               singularity caches                           #
# https://github.com/nf-core/tools#downloading-pipelines-for-offline-use     #
##############################################################################

#set cache dir
export NXF_SINGULARITY_CACHEDIR=/exports/igmm/eddie/BioinformaticsResources/nfcore/singularity-images/

##############################################################################
#                               RNA-SEQ                                      #
##############################################################################
nf-core download -r 3.0 rnaseq --force --container singularity
nf-core download  rnaseq -r 3.2 --container singularity

#chipseq
nf-core download -r 1.2.1 chipseq --container singularity
nf-core download -r 1.2.2 chipseq --container singularity 

#hlatyping
nf-core download hlatyping -r 1.2 --container singularity

#atac
nf-core download atacseq -r 1.2.1 --container singularity


#sarek
nf-core download sarek -r 1.2.1 --container singularity


#viralrecon
nf-core download viralrecon -r 2.1 --container singularity
nf-core download viralrecon -r 2.3.1 --container singularity

#methyl-seq
nf-core download methylseq -r 1.6.1 --container singularity
