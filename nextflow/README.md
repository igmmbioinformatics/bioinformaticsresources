# Nextflow configuration files for eddie

https://git.ecdf.ed.ac.uk/igmmbioinformatics/nextflow-eddie

https://www.wiki.ed.ac.uk/display/ResearchServices/Bioinformatics

```
git clone git@git.ecdf.ed.ac.uk:igmmbioinformatics/nextflow-eddie.git
mv nextflow-eddie nextflow
```
